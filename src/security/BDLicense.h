#ifndef INCLUDE_H_BDLICENSE
#define INCLUDE_H_BDLICENSE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "containers/all.h"
#include "BDAes.h"

static const string sLicenseDecodeError = "License decode error: ";
static const string sLicenseExpired = "License expired: ";
static const string sLicenseInvalid = "License invalid: ";

SCHEMATIC(SLicense, FRM_TYPELIST::SLICENSE, (uint32_t miLibCode; CDate mdtExpire;), (miLibCode, mdtExpire))

enum class LICENSE { VALID = 0, EXPIRED = 1, NOMATCH = 2 };

static bool DecodeLicense(const string& inp_sKey, const string& inp_sLicenseText, SLicense& inp_sLicense)
{
	BDAes cAes(inp_sKey);
	return cAes.Decrypt(inp_sLicenseText, inp_sLicense);
}

static void MakeLicense(const string& inp_sKey, uint32_t inp_iCode, CDate inp_dtExpire, string& inp_sLicense)
{
	BDAes cAes(inp_sKey);
	SLicense sLicense;
	sLicense.miLibCode = inp_iCode;
	sLicense.mdtExpire = inp_dtExpire;

	cAes.Encrypt(sLicense, inp_sLicense);
}

static LICENSE ValidateLicense(const SLicense& inp_sLicense, uint32_t inp_iLibCode, CDate inp_dtCurrent)
{

	if (inp_sLicense.miLibCode != inp_iLibCode) { return LICENSE::NOMATCH; }
	if (inp_dtCurrent > inp_sLicense.mdtExpire) { return LICENSE::EXPIRED; }

	return LICENSE::VALID;
}

static LICENSE ValidateLicense(const string& inp_sKey, const string& inp_sLicenseText, uint32_t inp_iCode, SLicense& inp_sLicense)
{
	if (DecodeLicense(inp_sKey, inp_sLicenseText, inp_sLicense))
	{
		return ValidateLicense(inp_sLicense, inp_iCode, CDate::CurrentDate());
	}

	return LICENSE::NOMATCH;
}
#endif