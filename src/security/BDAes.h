#ifndef INCLUDE_H_BDAES
#define INCLUDE_H_BDAES

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include <aes.h>
#include <modes.h>
#include <osrng.h>
#include <base64.h>
#include <hex.h>
using namespace CryptoPP;

#include "containers/Schematic.h"

class BDAes
{
public:
	typedef BDAes _Myt;
	
	~BDAes(void) {}

	BDAes(SecByteBlock inp_cKey) :mcKey(inp_cKey) {}
	
	BDAes(const string& inp_sKey) :mcKey(0x00, KEYLENGTH) { SetKey(inp_sKey); }

	bool Decrypt(const string& inp_sCipher, IContainer& inp_sSchematic)
	{
		if (inp_sCipher.size() == 0) return false;

		byte bIV[BLOCKSIZE];
		string sIv(inp_sCipher, 0, HEXIV);
		StringSource(sIv, true, new HexDecoder(new ArraySink(bIV, BLOCKSIZE)));

		string sTemp;

		decryptor_type cDecryptor(mcKey, mcKey.size(), bIV);

		StringSource((byte*)inp_sCipher.c_str() + HEXIV, inp_sCipher.size() - HEXIV, true, new HexDecoder(new StreamTransformationFilter(cDecryptor, new StringSink(sTemp))));
		SPack sPack(sTemp.size());
		memcpy(sPack.position(), sTemp.c_str(), sTemp.size());

		return inp_sSchematic.Unpack(sPack);
	}

	void Encrypt(IContainer& inp_sSchematic, string& inp_sCipher)
	{
		byte bIV[BLOCKSIZE];
		mcRNG.GenerateBlock(bIV, BLOCKSIZE);

		string sEncoded, sEncrypted;
		encryptor_type cEncryptor(mcKey, mcKey.size(), bIV);

		SPack sPack(inp_sSchematic.PackSize());
		inp_sSchematic.Pack(sPack);
		sPack.reset();
		StringSource((byte*)sPack.position(), sPack.size(), true, new StreamTransformationFilter(cEncryptor, new StringSink(sEncoded)));
		StringSource(sEncoded, true, new HexEncoder(new StringSink(sEncrypted)));

		StringSource(bIV, BLOCKSIZE, true, new HexEncoder(new StringSink(inp_sCipher)));
		inp_sCipher += sEncrypted;
	}

	void GetKey(SecByteBlock& inp_cKey)
	{
		inp_cKey = mcKey;
	}

	void GetKey(string& inp_sKey)
	{
		ArraySource(mcKey, mcKey.size(), true, new Base64Encoder(new StringSink(inp_sKey)));
	}

	void NewKey(void)
	{
		mcRNG.GenerateBlock(mcKey, mcKey.size());
	}

	void SetKey(SecByteBlock inp_cKey)
	{
		mcKey = inp_cKey;
	}

	void SetKey(string inp_sKey)
	{
		StringSource(inp_sKey, true, new Base64Decoder(new ArraySink(mcKey, AES::MAX_KEYLENGTH)));
	}

private:
	typedef CTR_Mode< AES >::Decryption decryptor_type;
	typedef CTR_Mode< AES >::Decryption encryptor_type;

	enum :size_t { BLOCKSIZE = AES::BLOCKSIZE, HEXIV = 2 * BLOCKSIZE, KEYLENGTH = AES::MAX_KEYLENGTH };

	SecByteBlock			mcKey;
	AutoSeededRandomPool	mcRNG;
};
#endif
