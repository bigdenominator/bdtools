#ifndef INCLUDE_H_ALLSECURITY
#define INCLUDE_H_ALLSECURITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "security/BDAes.h"
#include "security/BDLicense.h"

#endif