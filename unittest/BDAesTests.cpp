#ifndef INCLUDE_H_BDAESTESTS
#define INCLUDE_H_BDAESTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "security/BDAes.h"
#include "containers/SmartMap.h"
#include "containers/SmartVector.h"

typedef CSmartMap<int, double> AesMap;
typedef CSmartPtr<AesMap> AesMapPtr;

SCHEMATIC(SAesTest, FRM_TYPELIST::STESTCASE, (CVectorDblPtr mcVectorPtr; AesMapPtr mcMapPtr;), (mcVectorPtr, mcMapPtr))

TEST(BDAesTest, Consistency)
{
	SAesTest sBefore;
	sBefore.mcVectorPtr = CreateSmart<CVectorDbl>(10);
	sBefore.mcMapPtr = CreateSmart<AesMap>();

	auto itrBefore(iterators::GetItr(sBefore.mcVectorPtr));
	LOOP(itrBefore)
	{
		itrBefore.value() = 2 * itrBefore.key();
	}

	sBefore.mcMapPtr->add(1, 1.5);
	sBefore.mcMapPtr->add(2, 2.5);

	string sCipher;

	BDAes mcAes("SAMPLE_KEY");
	mcAes.Encrypt(sBefore, sCipher);

	SAesTest sAfter;
	EXPECT_TRUE(mcAes.Decrypt(sCipher, sAfter));

	EXPECT_EQ(10, sAfter.mcVectorPtr->size());
	EXPECT_EQ(2, sAfter.mcMapPtr->size());

	auto itrAfter(iterators::GetItr(sAfter.mcVectorPtr));
	LOOP(itrAfter)
	{
		EXPECT_EQ(itrAfter.value(), 2 * itrAfter.key());
	}

	EXPECT_EQ(1.5, (*sAfter.mcMapPtr)[1]);
	EXPECT_EQ(2.5, (*sAfter.mcMapPtr)[2]);
}
#endif