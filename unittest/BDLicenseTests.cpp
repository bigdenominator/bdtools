#ifndef INCLUDE_H_BDLICENSETESTS
#define INCLUDE_H_BDLICENSETESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "security/BDLicense.h"

class BDLicenseTests : public testing::Test
{
public:
	SLicense	msLicense;

	void SetUp(void) {}
	void TearDown(void) {}
};

TEST_F(BDLicenseTests, ManualCheck)
{
	const string sKey("0HnuW2q5/E+feEQcOivYF/+TWuPaLDu9JKg18+53WN8=");
	const string sLicense("2BBFEC74A6D98124900191B1D2BC02AA68A23FB106672ADE05C470B317054B1327876128");

	EXPECT_TRUE(DecodeLicense(sKey, sLicense, msLicense));
	EXPECT_EQ(100, msLicense.miLibCode);
	EXPECT_TRUE(CDate(20160101) == msLicense.mdtExpire);
}

TEST_F(BDLicenseTests, Validation)
{
	const string sKey("0HnuW2q5/E+feEQcOivYF/+TWuPaLDu9JKg18+53WN8=");

	EXPECT_TRUE(DecodeLicense(sKey, "B2B321A342951FF3C0BBEE333F32582EC51A79439765F7916536C584F3ABEDEC5F5BE690", msLicense));
	EXPECT_EQ(LICENSE::VALID, ValidateLicense(msLicense, 100, 20160411));

	EXPECT_TRUE(DecodeLicense(sKey, "2BBFEC74A6D98124900191B1D2BC02AA68A23FB106672ADE05C470B317054B1327876128", msLicense));
	EXPECT_EQ(LICENSE::EXPIRED, ValidateLicense(msLicense, 100, 20160411));

	EXPECT_TRUE(DecodeLicense(sKey, "B2B321A342951FF3C0BBEE333F32582EC51A79439765F7916536C584F3ABEDEC5F5BE690", msLicense));
	EXPECT_EQ(LICENSE::NOMATCH, ValidateLicense(msLicense, 101, 20160411));
}
#endif